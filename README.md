
<div align=center><img width="auto" height="50" src="https://raw.githubusercontent.com/osroom/osroom/master/apps/static/admin/sys_imgs/osroom-logo.png" alt="osroom"/></div>

> 还在开发中...,请不要下载使用, 也不要pull requests. 
## OSRoom 开源Web系统
开源协议:BSD2

<div align=center><img width="auto" src="https://raw.githubusercontent.com/osroom/osroom/master/docs/imgs/dashboard.png" alt="osroom"/></div>
OSRoom介绍与文档: http://www.osroom.com

OSRoom Demo: http://demo.osroom.com

> 支持
- 插件开发，官方插件Github地址: https://github.com/osroom-plugins
- 主题开发，官方主题Github地址: https://github.com/osroom
> 基础功能
- Web 服务端Api, Restful api，简单修改即可做微信小程序Api.

> 初期想法
* 主项目osroom主要构建osroom核心部分，包括插件扩展功能. 更多的功能需求使用插件单独开发扩展，需要使用时导入到osroom主项目中，结合使用。
为何想这样做？贡献者和贡献者的精力有限。

网址: http://www.osroom.com 
